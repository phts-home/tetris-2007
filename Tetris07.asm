.model small
.386
.data
x0 dw 10
y0 dw 0
FieldX dw 10
FieldY dw 0

field	db 0,0,0,0,0,0,0,0,0,0
		db 0,0,0,0,0,0,0,0,0,0
		db 0,0,0,0,0,0,0,0,0,0
		db 0,0,0,0,0,0,0,0,0,0
		db 0,0,0,0,0,0,0,0,0,0
		db 0,0,0,0,0,0,0,0,0,0
		db 0,0,0,0,0,0,0,0,0,0
		db 0,0,0,0,0,0,0,0,0,0
		db 0,0,0,0,0,0,0,0,0,0
		db 0,0,0,0,0,0,0,0,0,0
		db 0,0,0,0,0,0,0,0,0,0
		db 0,0,0,0,0,0,0,0,0,0
		db 0,0,0,0,0,0,0,0,0,0
		db 0,0,0,0,0,0,0,0,0,0
		db 0,0,0,0,0,0,0,0,0,0
f1	db 0,0,0,0
	db 0,0,0,0
	db 0,1,0,0
	db 1,1,1,0

.code
main proc
mov ax,@data
mov ds,ax

;---- ������������� ����� ----
mov ah,0		;����� ������� ��������� �����������	
mov al,3		;����� �����������
int 10h			;����� ���������� ������������
mov ax,0b800h	;�������� �������� �����������
mov es,ax		;�������� �������� �����������

;---- �������� ��������� ----
Draw:
	call DrwField
	call DrwFigure1
	
	call Delay
	
	inc y0
	
	cmp y0,12
	jne Draw
	call EndMoving
jmp Draw

;---- !!! TODO !!! ----
;---- ���������� ���� ----
DrwField proc near
	mov bx,0		;������ ��������
	mov cx,15		;8 == ���������� �����
	mov si,FieldX
	shl si,1		;si*2
	DF1:
		push cx
		mov cx,10	;6 == ���������� ��������
		DF2:
			cmp field[bx],0
			je DF_IsNill
			mov ax,5d51h
			mov es:[si],ax
			jmp DF_Cont
			DF_IsNill:
				mov ax,87b1h
				mov es:[si],ax
			DF_Cont:
			inc bx
			add si,2
		loop DF2
		pop cx
		add si,140
	loop DF1
	ret
DrwField endp

;---- !!! TODO !!! ----
;---- ���������� �������1 ----
DrwFigure1 proc near
	mov ax,160	;����������� ��������� ������� �� ������� si=y0*160+x0*2
	mul y0		;-//-
	mov bx, x0	;-//-
	shl bx,1	;-//-
	add ax,bx	;-//-
	mov si,ax	;-//-

	mov bx,0	;������ ��������
	mov cx,4	;4 == ���������� �����
	M1:
		push cx
		mov cx,4
		M2:
			cmp f1[bx],0
			je DrwFigure1_Cont
			mov ax,0d554h
			mov es:[si],ax
			DrwFigure1_Cont:
			inc bx
			add si,2
		loop M2
		pop cx
		add si,152
	loop M1
	ret
DrwFigure1 endp

;----- �������� -----
Delay proc near
	mov cx,0ffffh
	D1:
		call WaitKey
		push cx
		mov cx,1fffh
		D2:
		loop D2
		pop cx
	loop D1
	ret
Delay endp
;----- ����� �������� -----

;---- !!! TODO !!! ----
;---- ����� �������� �������1 ----
EndMoving proc near
	mov ax,10		;����������� �������� � ������� �� ������� si=y0*10+x0-FieldX
	sub y0,1		;-//-
	mul y0			;-//-
	add ax,x0		;-//-
	sub ax,FieldX	;-//-
	mov si,ax		;-//-
	
	mov di,0
	mov cx,16

	EM1:
		cmp f1[di],1
		jne EM_Skip1
		mov field[si],1
		
		EM_Skip1:
		inc si
		inc di
		cmp di,4
		je EM_L1
		cmp di,7
		je EM_L1
		cmp di,11
		je EM_L1
		jne EM_Skip2
		EM_L1:
		add si,6
		EM_Skip2:
	loop EM1

	mov x0,10
	mov y0,0
	ret
EndMoving endp

;---- ���������� ������ ----
WaitKey proc near
	mov ah,0bh		;��������� ������ ����������
	int 21h			;��������� ������ ����������
	cmp al,0ffh		;���� ��� ������� �������
	jne EndWaitKey	;�� ���������� ���������� �������

	mov ah,0		;��������� ������
	int 16h			;��������� ������
		cmp al,27		;Esc?
		jne ArRight		;���� ��� �� ����������
		jmp Exit
	ArRight:
		cmp ah,4Dh		;������� ������?
		jne ArLeft		;���� ��� �� ����������
		inc x0
		call DrwField
		call DrwFigure1
		jmp EndWaitKey
	ArLeft:
		cmp ah,4Bh		;������� �����?
		jne ArDown		;���� ��� �� ����������
		dec x0
		call DrwField
		call DrwFigure1
		jmp EndWaitKey
	ArDown:
		cmp ah,50h		;������� ����?
		jne ArUp		;���� ��� �� ����������
		inc y0
		call DrwField
		call DrwFigure1
		jmp EndWaitKey
	ArUp:
		cmp ah,48h		;������� �����?
		jne EndWaitKey	;���� ��� �� ����������
		dec y0
		call DrwField
		call DrwFigure1
	EndWaitKey:
	ret	
WaitKey endp

;---- ����� �� ��������� ----
Exit:
mov ax,4c00h
int 21h
main endp
end main